package net.lafox.robotDemo.commands;

import net.lafox.robotDemo.Command;
import net.lafox.robotDemo.data.Position;

public class CommandPosition implements Command {
    @Override
    public String getName() {
        return "POSITION";
    }

    @Override
    public Position exec(Position position, String... params) {
        return new Position(params[1],params[2],params[3]);
    }
}
