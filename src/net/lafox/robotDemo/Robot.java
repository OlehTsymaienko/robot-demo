package net.lafox.robotDemo;

import java.util.HashMap;
import java.util.Map;

import net.lafox.robotDemo.data.Position;

public class Robot {
    Map<String, Command> commands;
    private Position position;

    Robot(Command... commands) {
        this.commands = new HashMap<>();
        for (Command command : commands) {
            this.commands.put(command.getName(), command);
        }
    }

    void execScript(String script) {
        for (String line : script.split("\n")) {
            String[] params = line.split("\\s+");
            Command command = commands.get(params[0]);
            if (command==null){
                System.out.println("Command not found: " + params[0]);
                return;
            }
            position = command.exec(position, params);
        }
    }

    public Position getPosition() {
        return position;
    }

}
