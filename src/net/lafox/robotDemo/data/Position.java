package net.lafox.robotDemo.data;

public class Position {
    private final int x;
    private final int y;
    private final Direction direction;

    public Position(String x, String y, String direction) {
        this.x = Integer.parseInt(x);
        this.y = Integer.parseInt(y);
        this.direction = Direction.valueOf(direction);
    }

    @Override
    public String toString() {
        return "Position{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }
}
