package net.lafox.robotDemo.data;

public enum Direction {
    NORTH, EAST, SOUTH, WEST
}
