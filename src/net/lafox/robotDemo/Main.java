package net.lafox.robotDemo;

import net.lafox.robotDemo.commands.CommandPosition;

public class Main {
  static String script = "POSITION 1 3 EAST";
    public static void main(String[] args) {
        Robot robot = new Robot(
                new CommandPosition()
        );
        robot.execScript(script);
        System.out.println(robot.getPosition());
    }
}
