package net.lafox.robotDemo;

import net.lafox.robotDemo.data.Position;

public interface Command {
    String getName();
    Position exec(Position position, String... params);
}
